<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix' => 'api/v1/champions-league'], function () use ($router) {
    $router->post('results',  'ChampionsLeague\ResultController@create');
    $router->put('results',  'ChampionsLeague\ResultController@update');
});

$router->group(['prefix' => 'api/v2/champions-league'], function () use ($router) {
    $router->post('results',  'ChampionsLeague\V2\ResultController@create');
    $router->get('results',  'ChampionsLeague\V2\ResultController@index');
});
