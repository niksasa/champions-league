<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ChampionsLeagueResult extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'league_title',
        'matchday',
        'group',
        'home_team',
        'away_team',
        'kickoff_at',
        'score'
    ];

    /**
     * Store all results in database
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param array $results
     */
    public function scopeCreateAll($query, array $results)
    {
        DB::beginTransaction();

        foreach ($results as $result) {
            self::create([
                'league_title' => $result['leagueTitle'],
                'matchday' => $result['matchday'],
                'group' => $result['group'],
                'home_team' => $result['homeTeam'],
                'away_team' => $result['awayTeam'],
                'kickoff_at' => $result['kickoffAt'],
                'score' => $result['score']
            ]);
        }

        DB::commit();
    }

    /**
     * Update all results in database
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param array $results
     */
    public function scopeUpdateAll($query, array $results)
    {
        DB::beginTransaction();

        foreach ($results as $result) {
            $record = self::findByGroupHomeAndAwayTeams($result['group'], $result['homeTeam'], $result['awayTeam'])
                ->first();

            if ($record) {
                $record->update([
                    'league_title' => $result['leagueTitle'],
                    'matchday' => $result['matchday'],
                    'group' => $result['group'],
                    'home_team' => $result['homeTeam'],
                    'away_team' => $result['awayTeam'],
                    'kickoff_at' => $result['kickoffAt'],
                    'score' => $result['score']
                ]);
            } else {
                self::create([
                    'league_title' => $result['leagueTitle'],
                    'matchday' => $result['matchday'],
                    'group' => $result['group'],
                    'home_team' => $result['homeTeam'],
                    'away_team' => $result['awayTeam'],
                    'kickoff_at' => $result['kickoffAt'],
                    'score' => $result['score']
                ]);
            }
        }

        DB::commit();
    }

    /**
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param string|null $dateFrom
     * @param string|null $dateTo
     * @param string|null $group
     * @param string|null $team
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeFilter($query, string $dateFrom = null, string $dateTo = null, string $group = null, string $team = null)
    {
        $query->select('league_title', 'matchday', 'group', 'home_team', 'away_team', 'kickoff_at', 'score');

        if (!empty($dateFrom)) {
            $query->where('kickoff_at', '>=', $dateFrom);
        }

        if (!empty($dateTo)) {
            $query->where('kickoff_at', '<=', $dateTo);
        }

        if (!empty($group)) {
            $query->where('group', $group);
        }

        if (!empty($team)) {
            $query->where(function ($where) use ($team) {
                $where->where('home_team', '=', $team)
                    ->orWhere('away_team', '=', $team);
            });
        }

        return $query;
    }

    public function scopeFindByGroupHomeAndAwayTeams($query, $group, $homeTeam, $awayTeam)
    {
        $query->where('group', $group)
            ->where('home_team', $homeTeam)
            ->where('away_team', $awayTeam);

        return $query;
    }
}
