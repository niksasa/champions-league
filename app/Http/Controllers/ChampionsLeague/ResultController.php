<?php

namespace App\Http\Controllers\ChampionsLeague;

use App\ChampionsLeagueResult;
use App\Http\Controllers\Controller;
use App\Services\ChampionsLeague\ResultsTable;
use Illuminate\Http\Request;
use Validator;

class ResultController extends Controller
{
    private $resultsTable;
    /**
     * Create a new controller instance.
     *
     * @param \App\Services\ChampionsLeague\ResultsTable $resultsTable
     */
    public function __construct(ResultsTable $resultsTable)
    {
        $this->resultsTable = $resultsTable;
    }

    /**
     * Create results
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(Request $request)
    {
        $data = $request->all();

        $validator = $this->validateRequest($data);

        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'errors' => $validator->errors()
            ], 422);
        }

        $data['results'] = json_decode($request->get('results'), true);

        $validator = $this->validateResults($data);

        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'errors' => $validator->errors()
            ], 422);
        }

        // store results to database
        ChampionsLeagueResult::createAll($data['results']);

        // get results from database
        $results = ChampionsLeagueResult::all()->toArray();

        $groupFilters = !empty($data['groups'])
            ? json_decode($data['groups'])
            : null;

        return $this->resultsTable->generate($results, $groupFilters);

    }

    /**
     * Update results
     *
     * @param \Illuminate\Http\Request $request
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function update(Request $request)
    {
        $data = $request->all();

        $validator = $this->validateRequest($data);

        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'errors' => $validator->errors()
            ], 422);
        }

        $data['results'] = json_decode($request->get('results'), true);

        $validator = $this->validateResults($data);

        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'errors' => $validator->errors()
            ], 422);
        }

        // update results to database
        ChampionsLeagueResult::updateAll($data['results']);

        // get results from database
        $results = ChampionsLeagueResult::all()->toArray();

        $groupFilters = !empty($data['groups'])
            ? json_decode($data['groups'])
            : null;

        return $this->resultsTable->generate($results, $groupFilters);
    }

    /**
     * Validate request
     *
     * @param array $data
     * @return mixed
     */
    private function validateRequest(array $data)
    {
        return Validator::make($data, [
            'results' => 'required|json',
            'groups' => 'json'
        ]);
    }

    /**
     * Validate results
     *
     * @param array $data
     * @return mixed
     */
    private function validateResults(array $data)
    {
        return Validator::make($data, $this->rules($data));
    }

    /**
     * Generate validation rules
     *
     * @param array $data
     * @return array
     */
    private function rules(array $data)
    {
        $rules = ['results' => 'required|array'];

        if (is_array($data['results'])) {
            foreach ($data['results'] as $key => $val){
                $rules['results.'.$key.'.leagueTitle'] = 'required|string';
                $rules['results.'.$key.'.matchday'] = 'required|numeric';
                $rules['results.'.$key.'.group'] = 'required|string';
                $rules['results.'.$key.'.homeTeam'] = 'required|string';
                $rules['results.'.$key.'.awayTeam'] = 'required|string';
                $rules['results.'.$key.'.kickoffAt'] = 'required|date';
                $rules['results.'.$key.'.score'] = 'required|string|regex:"[0-9]:[0-9]"';
            }
        }

        return $rules;
    }
}
