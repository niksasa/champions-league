<?php

namespace App\Services\ChampionsLeague\V2;

use App\Interfaces\ChampionsLeague\ResultsTableInterface;

class ResultsTable implements ResultsTableInterface
{
    /**
     * Generate final results table
     *
     * @param array $results
     * @param null|array|string $groupsFilter
     * @param null|string $teamFilter
     * @return array
     */
    public function generate(array $results, $groupsFilter = null, $teamFilter = null)
    {
        $groups = $this->findAllGroupsWithTeams($results, $groupsFilter);

        $table = [];

        foreach ($groups as $group => $teams) {
            $row = [
                'leagueTitle' => $results[0]['league_title'],
                'group' => $group,
                'matchday' => 0,
                'standing' => []
            ];

            foreach ($teams as $team) {
                $standing = [
                    'team' => $team,
                    'playedGames' => 0,
                    'points' => 0,
                    'goals' => 0,
                    'goalsAgainst' => 0,
                    'goalDifference' => 0,
                    'win' => 0,
                    'lose' => 0,
                    'draw' => 0
                ];

                $matchday = 0;

                foreach ($results as $result) {
                    $score = explode(':', $result['score']);

                    switch ([$group, $team]) {
                        case [$result['group'], $result['home_team']]:
                            $standing['playedGames'] += 1;
                            $matchday += 1;

                            if ($score[0] > $score[1]) {
                                $standing['points'] += 3;
                                $standing['win'] += 1;
                            } elseif ($score[0] == $score[1]) {
                                $standing['points'] += 1;
                                $standing['draw'] += 1;
                            } else {
                                $standing['lose'] += 1;
                            }

                            $standing['goals'] += $score[0];
                            $standing['goalsAgainst'] += $score[1];

                            break;
                        case [$result['group'], $result['away_team']]:
                            $standing['playedGames'] += 1;
                            $matchday += 1;

                            if ($score[1] > $score[0]) {
                                $standing['points'] += 3;
                                $standing['win'] += 1;
                            } elseif ($score[1] == $score[0]) {
                                $standing['points'] += 1;
                                $standing['draw'] += 1;
                            } else {
                                $standing['lose'] += 1;
                            }

                            $standing['goals'] += $score[1];
                            $standing['goalsAgainst'] += $score[0];
                    }
                }

                $row['matchday'] = $matchday > $row['matchday']
                    ? $matchday
                    : $row['matchday'];

                $standing['goalDifference'] = $standing['goals'] - $standing['goalsAgainst'];

                $row['standing'][] = $standing;
            }

            $table[] = $row;
        }

        $this->sortResults($table);

        $this->addRanks($table);

        $this->filterByTeam($table, $teamFilter);

        return $table;

    }

    /**
     * Find all groups with teams
     *
     * @param array $results
     * @param null|array|string $groupsFilter
     * @return array
     */
    private function findAllGroupsWithTeams(array $results, $groupsFilter = null)
    {
        $groups = [];

        if ($groupsFilter && !is_array($groupsFilter)) {
            $groupsFilter = [$groupsFilter];
        }

        foreach ($results as $result) {
            if (!$groupsFilter || ($groupsFilter && in_array($result['group'], $groupsFilter))) {
                $groups[$result['group']][] = $result['home_team'];
                $groups[$result['group']][] = $result['away_team'];
            }
        }

        foreach ($groups as &$group) {
            $group = array_unique($group);
        }

        return $groups;
    }

    /**
     * Sort results
     *
     * @param array $table
     */
    private function sortResults(array &$table)
    {
        foreach ($table as &$value) {
            $length = count($value['standing']);

            for ($i = 0; $i < $length; $i++) {
                for ($j = $i + 1; $j < $length; $j++) {

                    if (($value['standing'][$j]['points'] > $value['standing'][$i]['points'])
                        || (($value['standing'][$j]['points'] == $value['standing'][$i]['points'])
                            && ($value['standing'][$j]['goals'] > $value['standing'][$i]['goals']))
                        || (($value['standing'][$j]['points'] == $value['standing'][$i]['points'])
                            && ($value['standing'][$j]['goals'] == $value['standing'][$i]['goals'])
                            && ($value['standing'][$j]['goalDifference'] > $value['standing'][$i]['goalDifference']))) {

                        $tmp = $value['standing'][$i];
                        $value['standing'][$i] = $value['standing'][$j];
                        $value['standing'][$j] = $tmp;
                    }
                }
            }
        }

    }

    /**
     * Add ranks
     *
     * @param array $table
     */
    private function addRanks(array &$table)
    {
        foreach ($table as &$value) {
            $i = 1;
            foreach ($value['standing'] as &$row) {
                $row['rank'] = $i;

                $i++;
            }
        }
    }

    /**
     * Filter results by team
     *
     * @param $table
     * @param null|string $team
     */
    private function filterByTeam(&$table, string $team = null)
    {
        if (empty($team)) {
            return;
        }

        foreach ($table as &$value) {
            $length = count($value['standing']);

            for ($i = 0; $i < $length; $i++) {

                if ($value['standing'][$i]['team'] != $team) {

                    unset ($value['standing'][$i]);
                }
            }
        }

    }
}
