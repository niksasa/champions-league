<?php

namespace App\Services\ChampionsLeague;

use App\Interfaces\ChampionsLeague\ResultsTableInterface;

class ResultsTable implements ResultsTableInterface
{
    /**
     * Generate final results table
     *
     * @param array $results
     * @param null|array|string $groupsFilter
     * @param null|string $teamFilter
     * @return array
     */
    public function generate(array $results, $groupsFilter = null, $teamFilter = null)
    {
        $groups = $this->findAllGroupsWithTeams($results, $groupsFilter);

        $table = [];
        foreach ($groups as $group => $teams) {
            foreach ($teams as $team) {
                $row = [
                    'leagueTitle' => $results[0]['league_title'],
                    'matchday' => 0,
                    'group' => $group,
                    'team' => $team,
                    'playedGames' => 0,
                    'points' => 0,
                    'goals' => 0,
                    'goalsAgainst' => 0,
                    'goalDifference' => 0,
                    'win' => 0,
                    'lose' => 0,
                    'draw' => 0
                ];

                foreach ($results as $result) {
                    $score = explode(':', $result['score']);

                    switch ([$group, $team]) {
                        case [$result['group'], $result['home_team']]:
                            $row['playedGames'] += 1;
                            $row['matchday'] += 1;

                            if ($score[0] > $score[1]) {
                                $row['points'] += 3;
                                $row['win'] += 1;
                            } elseif ($score[0] == $score[1]) {
                                $row['points'] += 1;
                                $row['draw'] += 1;
                            } else {
                                $row['lose'] += 1;
                            }

                            $row['goals'] += $score[0];
                            $row['goalsAgainst'] += $score[1];

                            break;
                        case [$result['group'], $result['away_team']]:
                            $row['playedGames'] += 1;
                            $row['matchday'] += 1;

                            if ($score[1] > $score[0]) {
                                $row['points'] += 3;
                                $row['win'] += 1;
                            } elseif ($score[1] == $score[0]) {
                                $row['points'] += 1;
                                $row['draw'] += 1;
                            } else {
                                $row['lose'] += 1;
                            }

                            $row['goals'] += $score[1];
                            $row['goalsAgainst'] += $score[0];
                    }
                }

                $row['goalDifference'] = $row['goals'] - $row['goalsAgainst'];

                $table[] = $row;
            }
        }

        $this->sortResults($table);

        $this->addRanks($table);

        return $table;
    }

    /**
     * Find all teams
     *
     * @param array $results
     * @param null|array|string $groupsFilter
     * @return array
     */
    private function findAllGroupsWithTeams(array $results, $groupsFilter = null)
    {
        $groups = [];

        if ($groupsFilter && !is_array($groupsFilter)) {
            $groupsFilter = [$groupsFilter];
        }

        foreach ($results as $result) {
            if (!$groupsFilter || ($groupsFilter && in_array($result['group'], $groupsFilter))) {
                $groups[$result['group']][] = $result['home_team'];
                $groups[$result['group']][] = $result['away_team'];
            }
        }

        foreach ($groups as &$group) {
            $group = array_unique($group);
        }

        return $groups;
    }

    /**
     * Sort results
     *
     * @param array $table
     */
    private function sortResults(array &$table)
    {
        $length = count($table);

        for ($i = 0; $i < $length; $i++) {
            for ($j = $i + 1; $j < $length; $j++) {

                if ($table[$j]['group'] != $table[$i]['group']) {
                    continue;
                }

                if (($table[$j]['points'] > $table[$i]['points'])
                    || (($table[$j]['points'] == $table[$i]['points']) && ($table[$j]['goals'] > $table[$i]['goals']))
                    || (($table[$j]['points'] == $table[$i]['points']) && ($table[$j]['goals'] == $table[$i]['goals'])
                        && ($table[$j]['goalDifference'] > $table[$i]['goalDifference']))) {

                    $tmp = $table[$i];
                    $table[$i] = $table[$j];
                    $table[$j] = $tmp;
                }
            }
        }
    }

    /**
     * Add ranks
     *
     * @param array $table
     */
    private function addRanks(array &$table)
    {
        $i = 1;
        $group = '';

        foreach ($table as &$row) {
            if ($group != $row['group']) {
                $i = 1;
            }

            $row['rank'] = $i;

            $group = $row['group'];

            $i++;
        }
    }
}
