<?php

namespace App\Interfaces\ChampionsLeague;

interface ResultsTableInterface
{
    /**
     * Generate final results table
     *
     * @param array $results
     * @param null|array|string $groupsFilter
     * @param null|string $teamFilter
     * @return array
     */
    public function generate(array $results, $groupsFilter = null, $teamFilter = null);
}
