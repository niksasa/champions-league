## Installation

Setup .env file in the project root directory according to the .env.example

Run composer update

```bash
composer update
```

Run database migration

```bash
php artisan migrate
```
