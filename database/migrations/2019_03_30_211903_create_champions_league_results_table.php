<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChampionsLeagueResultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('champions_league_results', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('league_title');
            $table->integer('matchday');
            $table->string('group');
            $table->string('home_team');
            $table->string('away_team');
            $table->dateTime('kickoff_at');
            $table->string('score');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('champions_league_results');
    }
}
